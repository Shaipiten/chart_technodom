import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Provider } from "react-redux";
import Main from "./screens/Main/Main";

const Root = ({ store }) => (
  <Provider store={store}>
    <Router>
      <Route path="/" exact component={Main} />
    </Router>
  </Provider>
);

export default Root;
