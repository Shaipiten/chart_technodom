import { combineReducers } from "redux";
import DateReducer from "./reducers/DateReducer"
import DoughnutReducer from "./reducers/DoughnutReducer"

const reducers = combineReducers({
    DateReducer,
    DoughnutReducer
})

export default reducers;
