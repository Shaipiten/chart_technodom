import { CLICK_DATE } from "../actions/DateAction";

export const initialState = {
    buttons: [
        {label: 'June', value: true },
        {label: 'July', value: false},
        {label: 'August', value: false },
        {label: 'September', value: false },
        {label: 'October', value: false },
        {label: 'November', value: false },
        {label: 'December', value: false },
        {label: 'January', value: false },
        {label: 'February', value: false },
        {label: 'March', value: false },
        {label: 'April', value: false },
        {label: 'May', value: false },
    ],
    id: 0
}

export default function DateReducer(state = initialState, action) {
    switch (action.type) {
        case CLICK_DATE: {
            const newData = state.buttons.map((element, i)=>{
                if(element.label === action.payload.label){
                    state.id = i
                    return {...element, value: true}
                }else{
                    return {...element, value: false}
                }
            })
            return {
                ...state,
                buttons: newData
            }
        }
        default:
            return state;
    }
}

