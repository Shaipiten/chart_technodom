import { CLICK_DOUGHNUT } from "../actions/DoughnutAction";

export const initialState = {
    item: 0
}

export default function DoughnutReducer(state = initialState, action) {
    switch (action.type) {
        case CLICK_DOUGHNUT: {
            return {
                ...state,
                item: action.payload
            }
        }
        default:
            return state;
    }
}