export const CLICK_DOUGHNUT = "DoughnutAction/CLICK_DOUGHNUT";

export const clickDoughnut = data => ({
    type: CLICK_DOUGHNUT,
    payload: data
});
     