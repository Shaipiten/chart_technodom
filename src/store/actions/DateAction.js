export const CLICK_DATE = "DateAction/CLICK_DATE";

export const clickDate = data => ({
    type: CLICK_DATE,
    payload: data
});
     