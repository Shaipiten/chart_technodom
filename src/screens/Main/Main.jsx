import React from 'react';
import './Main.sass';
import Header from '../../components/Header/Header'
import NavigationDate from '../../components/NavigationDate/NavigationDate'
import Doughnut from '../../components/Doughnut/Doughnut'

localStorage.setItem('data', JSON.stringify(
  {data:[
    { doughnut:[230, 280 ,470], in: 1400, out: 980, balance: 420}, 
    { doughnut:[500, 100 ,300], in: 1400, out: 900, balance: 920}, 
    { doughnut:[120, 550 ,340], in: 230, out: 1010, balance: 140 },
    { doughnut:[140, 250 ,600], in: 2000, out: 990, balance: 1150},
    { doughnut:[200, 300 ,700], in: 900, out: 1200, balance: 850},
    { doughnut:[300, 200 ,500], in: 300, out: 1000, balance: 150},
    { doughnut:[255, 300 ,200], in: 800, out: 755, balance: 195},
    { doughnut:[400, 500 ,100], in: 1300, out: 1000, balance: 495},
    { doughnut:[100, 150 ,300], in: 400, out: 550, balance: 345},
    { doughnut:[455, 110 ,350], in: 800, out: 915, balance: 230},
    { doughnut:[530, 200 ,430], in: 1000, out: 1160, balance: 70},
    { doughnut:[3000, 250 ,1330], in: 5000, out: 4580, balance: 490},
  ]}
))

function Main() {
  const deviceWidth= window.innerWidth
  const deviceHeight= window.innerHeight
  return (
    <div className="main" style={{height: deviceHeight, width: deviceWidth}}>
      <Header/>
      <NavigationDate/> 
      <Doughnut/>
    </div>
  );
}

export default Main;
