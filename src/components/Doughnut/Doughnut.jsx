import React from 'react';
import './Doughnut.sass';
import { connect } from "react-redux";
import { Doughnut } from "react-chartjs-2";
import {clickDoughnut} from "../../store/actions/DoughnutAction"
import {
    isMobile
} from "react-device-detect"

const labels = [
    {label: 'food and restaurants', img: 'food.png'},
    {label: 'travel expenses', img: 'flight.svg'},
    {label: 'other costs', img: 'coin.svg'},
]

const options ={
    animation: { animateScale: false, animateRotate: true },
    tooltips: { enabled: false },
    
    legend: {
        display: false,
        cursor: "pointer" 
    },
    cutoutPercentage: 70,
}

function DoughnutChart(props) {
    const localData = JSON.parse(localStorage.getItem('data'))
    const count = localData.data[props.data.id]
    const balance = count.balance
    const arr = count.doughnut
    const item = arr[props.item.item]
    const label = labels[props.item.item]
    const hght = window.innerHeight

    const data = (canvas) => {
        const ctx = canvas.getContext("2d")

        var grd=ctx.createLinearGradient(0, 0, 0 , 200);
        grd.addColorStop(0,"#FD3F2F");
        grd.addColorStop(1,"#FACE15");
        var grdTwo=ctx.createLinearGradient(0, 0, 0, 300);
        grdTwo.addColorStop(0,"#8D4DE8");
        grdTwo.addColorStop(1,"#FF2366");
        var grdThree=ctx.createLinearGradient(0, 0, 0, 300);
        grdThree.addColorStop(0,"#6956EC");
        grdThree.addColorStop(1,"#56B2BA")
        
        var grdgHover=ctx.createLinearGradient(0, 0, 0 , 200);
        grdgHover.addColorStop(1,"#E4BA13");
        grdgHover.addColorStop(0,"#CD3426");
        var grdTwoHover=ctx.createLinearGradient(0, 0, 0, 300);
        grdTwoHover.addColorStop(1,"#AC1844");
        grdTwoHover.addColorStop(0,"#6738A9");
        var grdThreeHover=ctx.createLinearGradient(0, 0, 0, 300);
        grdThreeHover.addColorStop(1,"#4D9FA6")
        grdThreeHover.addColorStop(0,"#5847C4");
        

        return {datasets: [{
            borderWidth: 0,
            data: arr,
            backgroundColor: [
                grd,
                grdTwo,
                grdThree
            ],
            hoverBackgroundColor: [
                grdgHover,
                grdTwoHover,
                grdThreeHover
            ]
        }]}
    };

    return (
        <div className="doughnut">
            <div className="doughnut_container">
                <div className="balance"><div className='balance_number'>${balance}</div><div className="balance_title">balance</div></div>
                <Doughnut data={data} options={options} height={isMobile? hght/4: 50} onElementsClick={elems => {
                    if(elems.length){
                        props.clickDoughnut(elems[0]._index)
                    }
                }}/>
            </div>
            <div className="title">
                <div className='title_number'>
                    <img src={`/img/${label.img}`}></img>
                    ${item}
                </div>
                <div>
                    {label.label}
                </div>  
            </div>
            <div className="statistic">
                <div className="statistic_container" style={isMobile? {margin: '10% auto', width: '100%', height: '100%'}: {}}>
                    <div className='in'>+${count.in} 
                        <img src="/img/up.png" alt=''></img>
                    </div>
                    <div className='out'>-${count.out}
                        <img src="/img/down.png" alt=''></img>
                    </div>
                </div>
            </div>
        </div>
    );
}


const mapStateToProps = state => {
    return {
      data: state.DateReducer,
      item: state.DoughnutReducer
    };
}; 

const mapDispatchToProps = dispatch => ({
    clickDoughnut: value => dispatch(clickDoughnut(value))
});

export default connect(mapStateToProps, mapDispatchToProps)(DoughnutChart);

