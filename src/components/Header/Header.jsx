import React from 'react';
import './Header.sass';

function Header() {
  return (
    <div className="header">
      <div className="header_content">
        <div className="header_container">
          Statistic
          <img src="/img/statistics.png" alt=''></img>
        </div>
      </div>
      
    </div>
  );
}

export default Header;
