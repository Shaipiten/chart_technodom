import React from 'react';
import { connect } from "react-redux";
import './NavigationDate.sass';
import { clickDate } from '../../store/actions/DateAction'
import {
    isMobile
} from "react-device-detect"

function NavigationDate(props) {

    function handleClick(elem){
        props.clickDate(elem)
    }
    return (
        <div className="navigation_date">
            <div className="container" style={isMobile? {width: '100%', overflowX: 'auto'}: {width: '80%'}}>
                {props.data.buttons.map((elem, i)=>(
                    <div key={i} onClick={()=>handleClick(elem)} className={`item ${elem.value? 'active': ''}`} style={isMobile? {padding: '0 20px', fontSize: 12, height: 'calc(100% - 2px)'}: {width: 'calc(100% / 12)'}}>
                        {elem.label}
                    </div>
                ))}
            </div>
        </div>
    );
}

const mapStateToProps = state => {
    return {
      data: state.DateReducer
    };
};

const mapDispatchToProps = dispatch => ({
    clickDate: value => dispatch(clickDate(value))
});

export default connect(mapStateToProps, mapDispatchToProps)(NavigationDate);
