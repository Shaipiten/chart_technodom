import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import reducers from './store/index'

export default createStore(reducers)
